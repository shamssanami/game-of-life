
public class Main  {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) {
	
		final int GRID_W = 100;
		final int GRID_H = 100;
		
		Model m = new Model(GRID_W,GRID_H);
		MyView v = new MyView(GRID_W,GRID_H);
		
		Controller controller = new Controller(m,v);
		//controller.startGame();
		//controller.upDateView();
		
		
	}
}
