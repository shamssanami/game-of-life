import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class MyView extends JFrame {

	JPanel mPanel;
	JPanel topPanel;
	JPanel bottomPanel;
	JPanel leftPanel;
	JPanel rightPanel;
	JPanel centerPanel;
	JFrame mFrame;
	JButton startButton;
	JButton[][] grid;
	
	public MyView(int w, int h){
		

		mFrame= new JFrame("Game Of Life");
		mFrame.setSize(w*5,h*5);
		mFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mFrame.setLocationRelativeTo(null);
		GridLayout gridLayout = new GridLayout(w,h);
		gridLayout.setHgap(1);
		gridLayout.setVgap(1);
		
		JPanel centerPanel = new JPanel(gridLayout);
		centerPanel.setSize(new Dimension(w,h));
		grid = new JButton[w][h];
		
		for(int r=0;r!=w;r++){
			for(int c=0;c!=h;c++){
				grid[r][c] = new JButton();
				grid[r][c].setIcon(new ImageIcon("graySmall.gif"));
				grid[r][c].setBorder(null);
				centerPanel.add(grid[r][c]);
			}
		}
		
		//JPanel buttonPanel = new JPanel();
		//centerPanel.setLayout(new FlowLayout());
		mFrame.add(centerPanel);
		//mFrame.add(buttonPanel);
		mFrame.setVisible(true);
	}

	
}
