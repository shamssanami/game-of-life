import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;


public class Controller {

	private Model model;
	private MyView myView;
	
	public Controller(Model m, MyView v){
		this.model = m;
		this.myView = v;
		initializeGrid();
		updateView();
	}	
	/**
	 * RULES:
	 *Any live cell with fewer than two live neighbours dies, as if caused by under-population.
	 *Any live cell with two or three live neighbours lives on to the next generation.
	 *Any live cell with more than three live neighbours dies, as if by overcrowding.
	 *Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction. 
	 * **/
		
	private void initializeGrid(){
		//int r, c =0;
		for( int r=0;r!=model.grid.length-1;r++){
			for(int c=0;c!=model.grid.length-1;c++){
		
				final int row = r;
				final int column = c;
				
				myView.grid[r][c].setBorder(null);
				myView.grid[r][c].addMouseListener(new MouseListener(){
			
					@Override
					public void mouseClicked(MouseEvent e) {
						
						myView.grid[row][column].setIcon(new ImageIcon("greeenSmall.gif"));
						model.grid[row][column] = 1;
						//myView.grid[row][column].repaint();
						updateView();
					}
					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
				});
			}
		}
	}
	
	public void initializeLife(int row, int column){
		
		//live cell 
		if(model.grid[row][column] == 1){
			if(getNeighborsCount(row,column) < 2 ){
				cellDies(row, column);
			}
			if(getNeighborsCount(row, column) == 2 ||
					getNeighborsCount(row, column) == 3 ){
				cellLives(row, column);
			}	
			if(getNeighborsCount(row, column) > 3){
				cellDies(row,column);
			}
		}
		//dead cell with 3 neighbors 
		if(model.grid[row][column] == 0 ){
			if(getNeighborsCount(row, column) == 3){
				cellLives(row,column);
			}
		 }
	}
		
	private int getNeighborsCount(int row, int column){
			
			int count = 0 ;
			//North neighbour 
			if(row -1 > 0){
				//there is no wall 
				if(model.grid[row -1][column] ==  1 ){
					count++;
				}
			}

			//South neighbour 
			if(row +1 < model.grid.length -1){
				if(model.grid[row + 1][column] == 1){
					count++;
				}
			}
			
			//East neighbour 
			if(column + 1 < model.grid.length-1){
				if(model.grid[row][column + 1] == 1){
					count++;
				}
			}
			
			//West neighbour 
			if(column -1 > 0){
				
				if(model.grid[row][column -1 ] == 1 ){
					count++;
				}
			}
			//North East neighbour 
			if(row-1 > 0 && column + 1 < model.grid.length-1){
				if(model.grid[row-1][column+1] ==1){
					count++;
				}
			}
			
			//North West neighbour 
			if(row-1 > 0 && column -1 > 0){
				if(model.grid[row-1][column-1] == 1 ){
					count++;
				}
			}
			//South East neighbour 
			if(row +1 < model.grid.length -1 && column+1 < model.grid.length-1){
				if(model.grid[row+1][column+1] ==  1){
					count++;
				}
			}
			//South West neighbour 
			if(row+1 < model.grid.length -1 && column -1 > 0){
				if(model.grid[row+1][column-1] == 1){
					count++;
				}
			}
			return count;
		}
		
		private void cellLives(int row, int column){
			model.grid[row][column] = 1;
		}
		
		private void cellDies(int row, int column){
			model.grid[row][column] = 0;
		}
		
		public int getGridWidth(){
			return model.grid.length-1;
		}
		
		public int getCellData(int row, int column){
			return model.grid[row][column];
		}

		public void updateView(){
			for(int row=0;row!=model.grid.length-1; row++){
				for(int column =0; column!=model.grid.length -1; column++){	
					if(model.grid[row][column] == 1){
						myView.grid[row][column].setIcon(new ImageIcon("greenSmall.gif"));
					}
					else{
						myView.grid[row][column].setIcon(new ImageIcon("graySmall.gif"));
					}
				}
			}
			myView.repaint();
		}
		
}
